/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { HttpAxiosClient } from '../../http-client'
import { load, Element, CheerioAPI } from 'cheerio'

interface CategoryNode {
  level: number
  order: number
  href?: string
  name?: string
  identity?: string
  children: CategoryNode[]
}

function processUlElement ($: CheerioAPI, node: CategoryNode) {
  return function (_ulOrder: number, ul: Element): void {
    $(ul).children('li').each(processLiElement($, node))
  }
}

function processLiElement ($: CheerioAPI, parentNode: CategoryNode) {
  return function (_i: number, li: Element): void {
    const categoryIdentity = li.attribs['data-cate']
    const categoryHref = load($(li).html()!)('a').attr('href')
    const categoryName = load($(li).html()!)('a > span').text().trim()
    const $categoryUl = load($(li).html()!)('ul')

    const node: CategoryNode = {
      level: parentNode.level + 1,
      order: _i + 1,
      children: []
    }

    if (typeof categoryHref === 'string') {
      node.href = categoryHref
    }

    if (typeof categoryName === 'string') {
      node.name = categoryName
    }

    if (typeof categoryIdentity === 'string') {
      node.identity = categoryIdentity
    }

    $categoryUl.each(processUlElement($, node))

    parentNode.children.push(node)
  }
}

export class Category {
  protected readonly httpClient = new HttpAxiosClient()

  public async getCategories (): Promise<CategoryNode[]> {
    const response = await this.httpClient.request<string>({
      method: 'GET',
      url: new URL('https://www.lazada.co.th')
    })

    const $ = load(response.data)

    const $categoryRoot = $('li#Level_1_Category_No1').last().parent()

    const nodes: CategoryNode[] = []
    $categoryRoot.children('ul').each((i, ul) => {
      const node: CategoryNode = {
        level: 0,
        order: i + 1,
        children: []
      }
      processUlElement($, node)(i, ul)
      nodes.push(node)
    })

    return nodes
  }
}
