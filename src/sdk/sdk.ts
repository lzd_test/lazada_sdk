import { Category } from './category'
import { Product } from './product'

export class SDK {
  public readonly category = new Category()
  public readonly product = new Product()
}
