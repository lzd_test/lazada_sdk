/* eslint-disable @typescript-eslint/no-explicit-any */
import { AxiosResponse } from 'axios'

export interface RequestParams {
  query: string
}

export interface HttpClientRequest {
  method: 'GET' | 'POST'
  url: URL
  data?: string
}

export interface HttpClientResponse<T> {
  headers: any
  status: AxiosResponse['status']
  statusText: AxiosResponse['statusText']
  data: T
}
