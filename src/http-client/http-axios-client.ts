/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import axios, { AxiosRequestConfig, AxiosError } from 'axios'

import {
  HttpClientRequest,
  HttpClientResponse
} from './interface'
import { HttpError } from '../errors'

/**
 * HttpClient wraps Axios and provides transparent data transfering between your code and clickhouse server
 * It uses HTTP/1 protocol
 */
export class HttpAxiosClient {
  readonly #axios = axios

  /**
   * Make full axios request and get full Clickhouse HTTP response
   *
   * @param {HttpClientRequest} config request config
   * @returns {Promise<HttpClientResponse>}
   */
  public async request<T>({
    method,
    url,
    data = ''
  }: HttpClientRequest): Promise<HttpClientResponse<T>> {
    const config: AxiosRequestConfig = {
      maxBodyLength: Infinity,
      method,
      url: url.toString(),
      data,
      validateStatus: () => true
    }

    const response = await this.#axios
      .request(config)
      .catch((error: AxiosError) => {
        throw new HttpError({
          message: error.response?.data as string,
          status: error.response?.status as number,
          statusText: error.response?.statusText as string,
          headers: error.response?.headers
        })
      })

    return {
      headers: response.headers,
      data: response.data,
      status: response.status,
      statusText: response.statusText
    }
  }
}
